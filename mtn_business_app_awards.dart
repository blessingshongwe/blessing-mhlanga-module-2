/* Write a basic program that stores and then prints the following data:
Your name, favorite app, and city;
Create an array to store all the winning apps of the MTN Business App of the 
Year Awards since 2012; 
a) Sort and print the apps by name;  
b) Print the winning app of 2017 and the winning app of 2018.; 
c) the Print total number of apps from the array.

Create a class and a) then use an object to print the name of the app,
sector/category, developer, and the year it won MTN Business App of the Year Awards. 
b) Create a function inside the class, transform the app name to all capital 
letters and then print the output. */

void main() {
  List<String> winners = [
    /* 2012 */ 'FNB',
    /* 2013 */ 'SnapScan',
    /* 2014 */ 'LIVE Inspect',
    /* 2015 */ 'WumDrop',
    /* 2016 */ 'Domestly',
    /* 2017 */ 'Shyft',
    /* 2018 */ 'Khula ecosystem',
    /* 2019 */ 'Naked Insurance',
    /* 2020 */ 'EasyEquities',
    /* 2021 */ 'Ambani',
  ];

  // sort list alphabetically

  // print(winners.sort());
  print("Total apps: ${winners.length}");
  print('2017 winner was: ${winners[2]}');
  print('2018 winner was: ${winners[6]}');
}
